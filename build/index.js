'use strict';

var _restify = require('restify');

var _restify2 = _interopRequireDefault(_restify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dashboards = [];
var names = Array('one', 'two', 'three');
var data = Array(3);

var server = _restify2.default.createServer();

function corsHandler(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,DELETE,POST,OPTIONS');
    res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
    res.setHeader('Access-Control-Max-Age', '1000');
    return next();
}

function optionsRoute(req, res, next) {
    res.send(200);
    return next();
}

server.use(_restify2.default.CORS({
    credentials: true, // defaults to false
    methods: ['GET', 'PUT', 'DELETE', 'POST', 'OPTIONS']
}));
server.use(_restify2.default.bodyParser());
server.use(_restify2.default.queryParser());

server.opts('/\.*/', corsHandler, optionsRoute);

server.get('/dashboard', function (req, res, next) {
    res.send(dashboards);
    return next();
});

server.post('/dashboard', function (req, res, next) {
    var _req$params = req.params,
        height = _req$params.height,
        width = _req$params.width,
        type = _req$params.type;

    var names = Array('one', 'two', 'three');
    var data = Array(3).map(function (ignore, index) {
        return {
            name: names[index],
            value: Math.floor(Math.random() * 20000)
        };
    });

    dashboards.push({ height: height, width: width, type: type, data: data });

    res.send(200, dashboards);
    return next();
});

server.put('/dashboard/:id', function (req, res, next) {
    var id = Number(req.params.id);

    if (isNaN(id) === true) {
        res.send(200, dashboards);
        return next();
    }

    var dLength = dashboards.length;

    if (id < 0 || id > dLength) {
        res.send(200, dashboards);
        return next();
    }

    var _req$params2 = req.params,
        height = _req$params2.height,
        width = _req$params2.width,
        type = _req$params2.type;

    var current = dashboards[id];
    dashboards[id] = Object.assign({}, current, { height: height, width: width, type: type });

    res.send(200, dashboards);
    return next();
});

server.del('/dashboard/:id', function (req, res, next) {
    var id = Number(req.params.id);

    if (isNaN(id) === true) {
        res.send(200, dashboards);
        return next();
    }

    var dLength = dashboards.length;

    if (id < 0 || id > dLength) {
        res.send(200, dashboards);
        return next();
    }

    dashboards.splice(id, 1);

    res.send(200, dashboards);
    return next();
});

server.get('/data', function (req, res, next) {
    var response = Array(3).fill().map(function (_, index) {
        return {
            name: names[index],
            value: Math.floor(Math.random() * 20000)
        };
    });
    console.log(response);
    res.send(200, response);
    return next();
});

server.listen(8080);