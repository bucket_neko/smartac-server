'use strict';

import restify from 'restify';

const dashboards = [];
const names = Array('one', 'two', 'three');
let data = Array(3)

const server = restify.createServer();

function corsHandler(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,DELETE,POST,OPTIONS');
    res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
    res.setHeader('Access-Control-Max-Age', '1000');
    return next();
}

function optionsRoute(req, res, next) {
    res.send(200);
    return next();
}

server.use(restify.CORS({
    credentials: true,                 // defaults to false
    methods: ['GET','PUT','DELETE','POST','OPTIONS']
}));
server.use(restify.bodyParser());
server.use(restify.queryParser());

server.opts('/\.*/', corsHandler, optionsRoute);

server.get('/dashboard', (req, res, next) => {
    res.send(dashboards);
    return next();
});

server.post('/dashboard', (req, res, next) => {
    let {height, width, type} = req.params;
    let names = Array('one', 'two', 'three');
    let data = Array(3).map( (ignore, index) => {
        return {
            name: names[index],
            value: Math.floor(Math.random()* 20000)
        };
    });
    
    dashboards.push( { height, width, type, data } );
    
    res.send(200, dashboards);
    return next();
});

server.put('/dashboard/:id', (req, res, next) => {
    let id = Number(req.params.id);
    
    if (isNaN(id) === true) {
        res.send(200, dashboards);
        return next();
    }
    
    let dLength = dashboards.length;
    
    if (id < 0 || id > dLength) {
        res.send(200, dashboards);
        return next();
    }
    
    let {height, width, type} = req.params;
    let current = dashboards[id];
    dashboards[id] = Object.assign({},current,{height, width, type});
    
    res.send(200, dashboards);
    return next();
});

server.del('/dashboard/:id', (req, res, next) => {
    let id = Number(req.params.id);
    
    if (isNaN(id) === true) {
        res.send(200, dashboards);
        return next();
    }
    
    let dLength = dashboards.length;
    
    if (id < 0 || id > dLength) {
        res.send(200, dashboards);
        return next();
    }
    
    dashboards.splice(id, 1);

    res.send(200, dashboards);
    return next();
});

server.get('/data', (req, res, next) => {
    let response = Array(3).fill().map( (_, index) => {
        return {
            name: names[index],
            value: Math.floor(Math.random()* 20000)
        };
    });
    console.log(response);
    res.send(200, response);
    return next();
});

server.listen(8080);